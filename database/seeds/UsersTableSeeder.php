<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'name'=>'Ismoil Shifoev',
          'email'=> 'ismail94.94@mail.ru',
          'password'=>bcrypt('secret')
        ]);
    }
}
